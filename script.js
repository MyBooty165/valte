const submitBtn = document.getElementById('submit-btn');
const passcodeInput = document.getElementById('passcode-input');

submitBtn.addEventListener('click', function() {
  const passcode = passcodeInput.value;
  // Check if the passcode is correct and redirect to the specified site
  if (passcode === 'FallBack') {
    window.location.href = 'https://docs.google.com/document/d/1BzxB5kMpAgVnIAD_SEqUChm2T6_JB3-kXhIgn7GmfvY/edit';
  } else {
    alert('Access Denied.');
  }
});
